// +----------------------------------------------------------------------
// | JavaWeb_Cloud_Pro微服务旗舰版框架 [ JavaWeb ]
// +----------------------------------------------------------------------
// | 版权所有 2019~2020 南京JavaWeb研发中心
// +----------------------------------------------------------------------
// | 官方网站: http://www.javaweb.vip/
// +----------------------------------------------------------------------
// | 作者: 鲲鹏 <1175401194@qq.com>
// +----------------------------------------------------------------------

package com.javaweb.common.framework.constants;

/**
 * 服务相关常量
 */
public class ServiceNameConstants {

    /**
     * 认证服务的
     */
    public static final String AUTH_SERVICE = "javaweb-auth";

    /**
     * 系统模块的
     */
    public static final String SYSTEM_SERVICE = "javaweb-system";

}
