// +----------------------------------------------------------------------
// | JavaWeb_Cloud_Pro微服务旗舰版框架 [ JavaWeb ]
// +----------------------------------------------------------------------
// | 版权所有 2019~2020 南京JavaWeb研发中心
// +----------------------------------------------------------------------
// | 官方网站: http://www.javaweb.vip/
// +----------------------------------------------------------------------
// | 作者: 鲲鹏 <1175401194@qq.com>
// +----------------------------------------------------------------------

package com.javaweb.service.system.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.javaweb.common.framework.common.BaseQuery;
import com.javaweb.common.framework.utils.JsonResult;
import com.javaweb.common.framework.utils.StringUtils;
import com.javaweb.common.security.common.BaseServiceImpl;
import com.javaweb.service.system.entity.ConfigGroup;
import com.javaweb.service.system.mapper.ConfigGroupMapper;
import com.javaweb.service.system.query.ConfigGroupQuery;
import com.javaweb.service.system.service.IConfigGroupService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 配置分组表 服务实现类
 * </p>
 *
 * @author 鲲鹏
 * @since 2020-11-06
 */
@Service
public class ConfigGroupServiceImpl extends BaseServiceImpl<ConfigGroupMapper, ConfigGroup> implements IConfigGroupService {

    @Autowired
    private ConfigGroupMapper configGroupMapper;

    /**
     * 获取配置分组列表
     *
     * @param query 查询条件
     * @return
     */
    @Override
    public JsonResult getList(BaseQuery query) {
        ConfigGroupQuery configGroupQuery = (ConfigGroupQuery) query;
        // 查询条件
        QueryWrapper<ConfigGroup> queryWrapper = new QueryWrapper<>();
        // 配置分组名称
        if (!StringUtils.isEmpty(configGroupQuery.getName())) {
            queryWrapper.like("name", configGroupQuery.getName());
        }
        queryWrapper.eq("mark", 1);
        queryWrapper.orderByAsc("sort");

        // 查询分页数据
        IPage<ConfigGroup> page = new Page<>(configGroupQuery.getPage(), configGroupQuery.getLimit());
        IPage<ConfigGroup> pageData = configGroupMapper.selectPage(page, queryWrapper);
        return JsonResult.success(pageData);
    }
}
